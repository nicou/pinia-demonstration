import { ref } from 'vue'

export const runTip = (defaultTip = '') => {
  const tipNow = ref(defaultTip)

  const setTip = (tip) => {
    tipNow.value = tip
  }

  return { setTip, tipNow }
}
export const tip1 = `
1. 向store添加新属性
2. 定义store时添加新选项
3. 向store添加新方法
4. 包装现有方法
5. 更改甚至取消操作
6. 实现本地存储等副作用
7. 针对特定的store应用进行操作
`
export const tip2 = `
如果不想同步改变，而是在各自的实例里变化
只需要不使用响应式 Api 创建动态属性即可
`
export const tip3 = `
通过返回对象来向所有商店添加动态属性 plugin
`

export const pluginStoreCode = `
import { defineStore } from 'pinia'
export const pluginStore = defineStore({
  id: 'plugin',
})
`

export const mixinCode = `
const storageKey = 'pinia'
// 向所有的store混入一个值
const plugin = ref(0)
export const piniaPlugin = ({ store, options }) => {
  store['plugin'] = plugin
  // 对plugin数值的更改进行监听
  watch(
    () => store.plugin,
    (data) => {
      // 当 pluginStore 改变时进行更新
      if (options.id === "plugin") {
        // 更新本地存储值
        updateStorage(storageKey, data * 10)
      }
    },
    { immediate: true, deep: true }
  )
}
`

export const appPluginCode = `
// pinia 插件注入
import { piniaPlugin } from '@/plugins/plugin.pinia'
const pinia = createPinia()
pinia.use(piniaPlugin)
`

export const htmlCode = `
// store
const pluginStoreCase = pluginStore()
const asyncStoreCase = asyncStore()
// * 获取的 plugin 的值
const storePlugin = computed(() => pluginStoreCase.plugin)
const asyncPlugin = computed(() => asyncStoreCase.plugin)
`

export const addCode = `
// 只修改 pluginStore 的值
const add = () => { pluginStoreCase.plugin++ }
`

export const reduceCode = `
// 只修改 pluginStore 的值
const reduce = () => { pluginStoreCase.plugin-- }
`

export const getStorageCode = `
const fetchStorage = () => {
  storage.value = getStorage('pinia')
}
`